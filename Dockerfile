# Base image
FROM nginx:latest
EXPOSE 80

ENV PROJECT_USER "testapp"
ENV PROJECT_GROUP "testapp"

WORKDIR /home/$PROJECT_USER/app

# Create the user system user and group
RUN groupadd -r $PROJECT_USER \
    && useradd -r -g $PROJECT_GROUP $PROJECT_USER \
    && mkdir -p /home/$PROJECT_USER \
    && chown $PROJECT_USER:$PROJECT_GROUP /home/$PROJECT_USER

COPY ./application /home/$PROJECT_USER/app
RUN chown -R $PROJECT_USER:$PORJECT_GROUP /home/$PROJECT_USER/app

RUN sed -i "s/user  nginx;/user ${PROJECT_USER} ${PROJECT_GROUP};/" /etc/nginx/nginx.conf \
    && rm -rf /var/www

RUN rm -rf /etc/nginx/sites-available \
    && rm -rf /etc/nginx/sites-enabled

COPY ./default.conf /etc/nginx/conf.d/default.conf

